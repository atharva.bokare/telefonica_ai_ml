import pandas as pd
from matplotlib import pyplot as plt
import argparse
# import boto3
import psycopg2
import numpy as np
from sqlalchemy import create_engine
from statistics import stdev
from configparser import ConfigParser


class timeseries_metrics_calculation:

    def strip_spaces(self, a_str_with_spaces):

        return a_str_with_spaces.replace(' ', '')

    def format_column(self, num):
        return '{:.2f}'.format(num)

    # converters={'cn_origem': self.strip_spaces}
    def calculate_timeseries(self, config_file):
        config = ConfigParser()
        config.read(config_file)
        data=pd.to_datetime(config['fields']['timeseries'])
        print(data)
        current_time = pd.Timestamp.now()
        print(current_time)

        path = config['filepaths']['input']

        field1 = config['fields']['preprocess']
        # field1 = field1.split(',')
        kpi_field2 = config['fields']['kpi']
        kpi_field2 = kpi_field2.split(',')

        conn_str = config['dbconn']['conn_string']
        # exit(0)

        conn_string = conn_str
        db = create_engine(conn_string)

        # exit(0)
        conn = db.connect()

        df = pd.read_csv(path, converters={field1: self.strip_spaces})
        # df=df.sort_values(by='data', ascending=False)

        df = df.dropna(subset=[field1])

        tmp = []

        global agg_new_trans
        for kpi in kpi_field2:
            agg_func = {kpi: ['mean', 'var', 'std']}
            agg_new_trans = df.groupby(field1).agg(agg_func)
            agg_new_trans['kpi_name'] = kpi
            agg_new_trans.drop(agg_new_trans.index[:3], inplace=True)
            tmp.append(agg_new_trans)

        create_flag = False
        try:
            for df in tmp:
                df.columns = ['mean', 'var', 'std', 'kpi_name']
                df['warning_threshold'] = df['mean'] + 2 * df['std']
                df['critical_threshold'] = df['mean'] + 3 * df['std']
                df['warning_threshold'] = df['warning_threshold'].apply(self.format_column)
                df['critical_threshold'] = df['critical_threshold'].apply(self.format_column)
                # print(df)
                if not create_flag:
                    df.to_sql('timeseries_metric_calculation_table', conn, if_exists='replace')
                    create_flag = True
                else:
                    df.to_sql("timeseries_metric_calculation_table", conn, if_exists='append')
        except Exception as e:
            raise e
